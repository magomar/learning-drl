from gymnasium import register

from gym_examples.envs.grid_world import GridWorldEnv


def register_gridworld(max_episode_steps=1000):
    register(
        id='GridWorld-v0',
        entry_point='gym_examples.envs:GridWorldEnv',
        max_episode_steps=max_episode_steps,
    )
