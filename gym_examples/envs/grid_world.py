from __future__ import annotations

from dataclasses import dataclass, field, InitVar

import numpy as np
import pygame
from gymnasium import Env
from gymnasium.spaces import Box, Discrete, Dict

actions = {
    0: np.array([0, 1]),  # Up
    1: np.array([1, 0]),  # Right
    2: np.array([0, -1]),  # Down
    3: np.array([-1, 0]),  # Left
}

rewards = {
    'success': 10,
    'failure': -10,
    'other': -1
}

action_names = {
    0: 'Up',
    1: 'Right',
    2: 'Down',
    3: 'Left'
}


@dataclass(eq=False, repr=False)
class GridObservationSpace(Dict):
    agent: Box = field(init=False)
    target: Box = field(init=False)
    wall: Box = field(init=False)
    pit: Box = field(init=False)
    size: InitVar[int]

    def __post_init__(self, size: int):
        self.agent = Box(0, size - 1, shape=(2,), dtype=np.int64)
        self.target = Box(0, size - 1, shape=(2,), dtype=np.int64)
        self.wall = Box(0, size - 1, shape=(2,), dtype=np.int64)
        self.pit = Box(0, size - 1, shape=(2,), dtype=np.int64)

    @property
    def spaces(self):
        return self.__dict__


@dataclass(eq=False, repr=False)
class Grid:
    size: int
    np_random: np.random.Generator
    locations: dict[str, np.ndarray] = field(init=False, default_factory=dict)

    def __post_init__(self):
        self.size = min(max(self.size, 4), 10)
        self.assign_free_random_location('target')
        self.assign_free_random_location('wall')
        self.assign_free_random_location('pit')

    @property
    def agent_location(self) -> np.ndarray:
        return self.locations['agent']

    @property
    def target_location(self) -> np.ndarray:
        return self.locations['target']

    @property
    def wall_location(self) -> np.ndarray:
        return self.locations['wall']

    @property
    def pit_location(self) -> np.ndarray:
        return self.locations['pit']

    def random_location(self) -> np.ndarray:
        return self.np_random.integers(0, self.size, size=2, dtype=int)

    def assign_free_random_location(self, component: str):
        occupied = np.zeros((self.size, self.size), dtype=int)
        for key, location in self.locations.items():
            if key != component:
                occupied[tuple(location)] = 1
        new_location = self.random_location()
        while occupied[tuple(new_location)]:
            new_location = self.random_location()
        self.locations[component] = new_location

    def reset(self):
        # Choose the agent's location uniformly at random
        self.assign_free_random_location('agent')

    def move_agent(self, action: int):
        # Map the action (element of {0,1,2,3}) to the direction we walk in
        direction = actions[action]
        # We use `np.clip` to make sure we don't leave the grid
        new_agent_location = np.clip(self.agent_location + direction, 0, self.size - 1)
        # The wall blocks movement too
        if not np.array_equal(new_agent_location, self.wall_location):
            self.locations['agent'] = new_agent_location

    def observe(self) -> dict[str, any]:
        return self.locations

    def info(self) -> dict[str, any]:
        return {
            # the Manhattan distance between the agent and the target
            'distance': np.linalg.norm(self.agent_location - self.target_location, ord=1)
        }

    def succeeded(self) -> bool:
        return np.array_equal(self.agent_location, self.target_location)

    def failed(self) -> bool:
        return np.array_equal(self.agent_location, self.pit_location)

    def terminated(self) -> bool:
        return self.succeeded() or self.failed()

    def reward(self) -> int:
        if self.succeeded():
            return rewards['success']
        return rewards['failure'] if self.failed() else rewards['other']


class GridWorldEnv(Env):
    metadata = {'render_modes': ['human', 'rgb_array'],
                'render_fps': 10}

    def __init__(self, render_mode=None, size=5):
        super().__init__()
        self.window_size = 512  # The size of the PyGame window
        self.observation_space = GridObservationSpace(size)
        self.action_space = Discrete(4)
        assert render_mode is None or render_mode in self.metadata['render_modes']
        self.render_mode = render_mode
        self.window = None
        self.clock = None
        self._grid = Grid(size, self.np_random)

    @property
    def grid(self) -> Grid:
        return self._grid

    @property
    def num_observables(self) -> int:
        return len(self.observation_space.__dict__)

    def reset(self, seed=None, options=None):
        super().reset(seed=seed)
        self.grid.reset()

        if self.render_mode == 'human':
            self._render_frame()

        # reset returns initial observation and additional info
        return self.grid.observe(), self.grid.info()

    def step(self, action) -> tuple[dict, int, bool, bool, dict]:
        grid = self.grid
        grid.move_agent(action)

        if self.render_mode == 'human':
            self._render_frame()

        # return ``(observation, reward, done, truncated, info)``.
        return self.grid.observe(), self.grid.reward(), grid.terminated(), False, self.grid.info()

    def render(self):
        if self.render_mode == 'rgb_array':
            return self._render_frame()

    def _render_frame(self):
        if self.window is None and self.render_mode == 'human':
            pygame.init()
            pygame.display.init()
            self.window = pygame.display.set_mode((self.window_size, self.window_size))
        if self.clock is None and self.render_mode == 'human':
            self.clock = pygame.time.Clock()

        canvas = pygame.Surface((self.window_size, self.window_size))
        canvas.fill((255, 255, 255))
        pix_square_size = (
                self.window_size / self.grid.size
        )  # The size of a single grid square in pixels

        # Draw the target
        pygame.draw.rect(
            canvas,
            (0, 255, 0),
            pygame.Rect(
                pix_square_size * self.grid.target_location,
                (pix_square_size, pix_square_size),
            ),
        )
        # Draw the pit/fire
        pygame.draw.rect(
            canvas,
            (255, 0, 0),
            pygame.Rect(
                pix_square_size * self.grid.pit_location,
                (pix_square_size, pix_square_size),
            ),
        )
        # Draw the wall
        pygame.draw.rect(
            canvas,
            (50, 50, 50),
            pygame.Rect(
                pix_square_size * self.grid.wall_location,
                (pix_square_size, pix_square_size),
            ),
        )
        # Draw the agent
        pygame.draw.circle(
            canvas,
            (0, 0, 255),
            (self.grid.agent_location + 0.5) * pix_square_size,
            pix_square_size / 3,
        )

        # Finally, add some gridlines
        for x in range(self.grid.size + 1):
            pygame.draw.line(
                canvas,
                0,
                (0, pix_square_size * x),
                (self.window_size, pix_square_size * x),
                width=3,
            )
            pygame.draw.line(
                canvas,
                0,
                (pix_square_size * x, 0),
                (pix_square_size * x, self.window_size),
                width=3,
            )

        if self.render_mode != 'human':
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(canvas)), axes=(1, 0, 2)
            )
        # The following line copies our drawings from `canvas` to the visible window
        self.window.blit(canvas, canvas.get_rect())
        pygame.event.pump()
        pygame.display.update()

        # We need to ensure that human-rendering occurs at the predefined framerate.
        # The following line will automatically add a delay to keep the framerate stable.
        self.clock.tick(self.metadata['render_fps'])

    def close(self):
        if self.window is not None:
            pygame.display.quit()
            pygame.quit()
