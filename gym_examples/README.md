# Gymnasium Examples

Some simple examples of Gymnasium environments and wrappers.
For some explanations of these examples, see the [Gymnasium documentation](https://gymnasium.farama.org).

## Environments

This repository hosts examples derived from the ones that are shown 
[on the environment creation documentation](https://gymnasium.farama.org/tutorials/environment_creation/).

- `GridWorldEnv`: Simplistic implementation of gridworld environment

## Wrappers

This repository hosts the examples that are
shown [on wrapper documentation](https://gymnasium.farama.org/api/wrappers/).

- `ClipReward`: A `RewardWrapper` that clips immediate rewards to a valid range
- `DiscreteActions`: An `ActionWrapper` that restricts the action space to a finite subset
- `RelativePosition`: An `ObservationWrapper` that computes the relative position between an agent and a target
- `ReacherRewardWrapper`: Allow us to weight the reward terms for the reacher environment
