from gym_examples.envs import register_gridworld

register_gridworld(max_episode_steps=1000)
