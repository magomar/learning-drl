import random

import matplotlib.pyplot as plt
import numpy as np


def get_reward(arm_prob, n=10):
    return sum(random.random() < arm_prob for _ in range(n))


def softmax(average_reward_so_far, tau=1.12):
    return np.exp(average_reward_so_far / tau) / np.sum(np.exp(average_reward_so_far / tau))


def update_record(current_record, new_action, new_reward):
    new_record = (current_record[new_action, 0] * current_record[new_action, 1] + new_reward) / (
            current_record[new_action, 0] + 1)
    current_record[new_action, 0] += 1
    current_record[new_action, 1] = new_record
    return current_record


if __name__ == '__main__':
    k = 500  # Number of plays
    n = 10  # Number of slot machines (aka one_arm bandits), number of possible actions
    probs = np.random.rand(n)  # Prob associated to each slot machine
    print(f'Best machine gives an average reward of {np.max(probs) * n}')
    # 10 actions x 2 columns: Count #, Avg Reward
    record = np.zeros((n, 2))
    rewards = [0]
    for i in range(1, k):
        choice_probs = softmax(record[:, 1])
        choice = np.random.choice(np.arange(n), p=choice_probs)
        reward = get_reward(probs[choice])
        record = update_record(record, choice, reward)
        mean_reward = (i * rewards[-1] + reward) / (i + 1)
        rewards.append(mean_reward)
    fig, ax = plt.subplots(1, 1)
    ax.set_xlabel("Plays")
    ax.set_ylabel("Avg Reward")
    fig.set_size_inches(9, 5)
    ax.scatter(np.arange(len(rewards)), rewards)
    plt.show()
    print(f'Final mean reward: {rewards[-1]}')
