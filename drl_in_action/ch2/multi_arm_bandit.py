import random

import matplotlib.pyplot as plt
import numpy as np


def get_reward(arm_prob, n=10):
    return sum(random.random() < arm_prob for _ in range(n))


def get_best_arm(record):
    return np.argmax(record[:, 1], axis=0)


def update_record(current_record, new_action, new_reward):
    new_record = (current_record[new_action, 0] * current_record[new_action, 1] + new_reward) / (
            current_record[new_action, 0] + 1)
    current_record[new_action, 0] += 1
    current_record[new_action, 1] = new_record
    return current_record


if __name__ == '__main__':
    k = 500  # Number of plays
    n = 10  # Number of slot machines (aka one_arm bandits)
    probs = np.random.rand(n)  # Prob associated to each slot machine
    print(f'Best machine gives an average reward of {np.max(probs) * n}')
    eps = 1.0 / n
    # 10 actions x 2 columns: Count #, Avg Reward
    record = np.zeros((n, 2))
    rewards = [0]
    for i in range(1, k):
        if random.random() > eps:
            choice = get_best_arm(record)
        else:
            choice = np.random.randint(10)
        reward = get_reward(probs[choice])
        record = update_record(record, choice, reward)
        mean_reward = (i * rewards[-1] + reward) / (i + 1)
        rewards.append(mean_reward)
    fig, ax = plt.subplots(1, 1)
    ax.set_xlabel("Plays")
    ax.set_ylabel("Avg Reward")
    fig.set_size_inches(9, 5)
    ax.scatter(np.arange(len(rewards)), rewards)
    plt.show()
    print(f'Final mean reward: {rewards[-1]}')
