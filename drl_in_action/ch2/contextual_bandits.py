import random

import numpy as np
import torch
from matplotlib import pyplot as plt


class ContextBandit:
    def __init__(self, arms=10):
        self._arms = arms
        self._state = None
        # In the transition matrix each row represents a state, each column an action.
        # Here both states and actions correspond to "arms" (web pages/adverts in the book)
        self.transition_matrix = np.random.rand(arms, arms)
        self._update_state()

    @property
    def state(self):
        return self._state

    @property
    def arms(self):
        return self._arms

    def _update_state(self):
        self._state = np.random.randint(0, self.arms)

    def get_reward(self, arm):
        transition_prob = self.transition_matrix[self.state][arm]
        return sum(random.random() < transition_prob for _ in range(self.arms))

    def choose_arm(self, arm):
        reward = self.get_reward(arm)
        self._update_state()
        return reward


def build_model(input_size, output_size, hidden_layer_size):
    return torch.nn.Sequential(
        torch.nn.Linear(input_size, hidden_layer_size),
        torch.nn.ReLU(),
        torch.nn.Linear(hidden_layer_size, output_size),
        torch.nn.ReLU(),
    )


def softmax(y, tau=1.12):
    return np.exp(y / tau) / np.sum(np.exp(y / tau))


def one_hot(n, pos, val=1):
    one_hot_vec = np.zeros(n)
    one_hot_vec[pos] = val
    return one_hot_vec


def moving_average(a, n=50):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def train(env, model, epochs, learning_rate=1e-2):
    state = torch.Tensor(one_hot(env.arms, env.state))  # Obtains initial state
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    rewards = []
    for _ in range(epochs):
        y_pred = model(state)  # Obtains output using current weights (this is going to become the policy)
        av_softmax = softmax(y_pred.data.numpy(), tau=2.0)  # Converts to prob distribution
        av_softmax /= av_softmax.sum()  # And normalizes so that Sum(probs)  = 1    This becomes the policy function
        choice = np.random.choice(arms, p=av_softmax)  # Selects action based on policy
        reward = env.choose_arm(choice)  # Obtains rewards for selected action
        one_hot_reward = y_pred.data.numpy().copy()  # Converts output to numpy
        one_hot_reward[choice] = reward  # And creates one-hot representation to be used as training label
        rewards.append(reward)  # Saves reward to keep track
        loss = loss_fn(y_pred, torch.Tensor(one_hot_reward))  # Uses new reward to update loss function
        optimizer.zero_grad()
        loss.backward()  # Backprop
        optimizer.step()  # Modify weights
        state = torch.Tensor(one_hot(env.arms, env.state))  # Updates environment state
    return np.array(rewards)


if __name__ == '__main__':
    arms = 10
    hidden_layer_size = 100
    batch_size = 1
    epochs = 5000
    env = ContextBandit(arms=arms)
    model = build_model(arms, arms, hidden_layer_size)
    loss_fn = torch.nn.MSELoss()
    rewards = train(env, model, epochs)
    plt.plot(moving_average(rewards, n=50))
    plt.plot(moving_average(rewards, n=500))
    plt.show()
