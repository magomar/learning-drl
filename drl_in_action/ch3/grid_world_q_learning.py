import random
from dataclasses import dataclass, field

import gymnasium
import numpy as np
import torch
from matplotlib import pyplot as plt
from torch import nn, optim

from envs import GridWorldEnv
from envs.grid_world import action_names


def mask(location: np.ndarray, size: int) -> np.ndarray:
    mask = np.zeros((size, size), dtype=int)
    mask[tuple(location)] = 1
    return mask


@dataclass(unsafe_hash=True)
class GridWorldQNet:
    """
    Q(St,At) = Q(St,At) + alpha[Rt+1 + lambda max{Q(St+1,a)} - Q(St,At)]
    """
    env: GridWorldEnv
    seed: int = None
    model: nn.Module = field(init=False)
    optimizer: optim.Optimizer = field(init=False)
    loss_fn: nn.Module = field(init=False)
    input_size: int = field(init=False)
    hidden_1_size: int = 150
    hidden_2_size: int = 100
    output_size: int = 4
    gamma: float = 0.9
    learning_rate: float = 1e-3
    epsilon: float = 1.0

    def __post_init__(self):
        super().__init__()
        self.input_size = self.env.grid.size * self.env.grid.size * len(self.env.observation_space.spaces)
        self.model = nn.Sequential(
            nn.Linear(self.input_size, self.hidden_1_size),
            nn.ReLU(),
            nn.Linear(self.hidden_1_size, self.hidden_2_size),
            nn.ReLU(),
            nn.Linear(self.hidden_2_size, self.output_size)
        )
        self.optimizer = optim.Adam(self.model.parameters(), lr=self.learning_rate)
        self.loss_fn = nn.MSELoss()

    @property
    def size(self) -> int:
        return self.env.grid.size

    def preprocess(self, observation: dict[str, any], noise=False) -> np.ndarray:
        # Convert observation to numpy array, with batch size = 1
        batch = np.concatenate([mask(location, self.size).flatten() for
                                location in observation.values()]).reshape(1, self.input_size).astype(float)
        if noise:  # Add some noise, a form of regularization
            batch += np.random.rand(1, self.input_size) / 10.0
        return batch

    def train(self, games: int):
        losses = []
        average_reward = 0
        average_steps = 0
        for i in range(games):
            game_reward = 0
            steps = 0
            observation, info = self.env.reset(seed=self.seed)
            terminated, truncated = False, False
            # Represent current state as a tensor (a batch of 1 x grid_size)
            state1 = torch.from_numpy(self.preprocess(observation, noise=True)).float()
            while not terminated:
                # Obtain Q-value (action-values) for all possible actions for the current state
                qval = self.model(state1)
                # Choose action as the one with maximum Q, or randomly (epsilon greedy approach)
                action = (np.random.randint(0, 4)
                          if random.random() < self.epsilon
                          else np.argmax(qval.data.numpy()))
                # Apply action, reach new statue (observation) and obtain reward
                observation, reward, terminated, truncated, info = self.env.step(action)
                game_reward += reward
                # Represent the new state as a tensor
                state2 = torch.from_numpy(self.preprocess(observation)).float()  # New state as numpy batch
                # Compute all Q-values for the new state and get the maximum value
                with torch.no_grad():
                    # no grad because we don't want to apply "gradient descent" algorithm based on state2, only state1
                    # we train based on state1, state2 is just creating the target (the label) for next step
                    # no grad prevents the creation of computational graph
                    new_qval = self.model(state2)
                max_qval = torch.max(new_qval)
                # Compute target Q-value as Y = reward + discount * max_Q-value
                target_q = reward if terminated else reward + (self.gamma * max_qval)
                target_q = torch.Tensor([target_q]).detach()  # Detach the tensor from the computational graph
                #  Note his is actually not needed, since we have already specified no_grad
                # Current Q-value of the action chosen, the one we want to improve
                estimated_q = qval.squeeze()[action]
                # Compute error between the improved estimation of Q-value and the current Q-value
                loss = self.loss_fn(estimated_q, target_q)
                # Set gradients to zero before backprop
                self.optimizer.zero_grad()
                # Backprop step
                loss.backward()
                # Save error to later check the training
                losses.append(loss.item())
                self.optimizer.step()
                state1 = state2
                steps += 1
            average_reward = ((average_reward * i) + game_reward) / (i + 1)
            average_steps = ((average_steps * i) + steps) / (i + 1)
            won = game_reward == 10
            terminated = 'WON' if won else 'LOST'
            print(f'Game {i} {terminated} after {steps} steps:'
                  f'   - Reward = {game_reward},'
                  f'   - Average reward = {round(average_reward, 1)}'
                  f'   - Average steps = {round(average_steps, 1)}')
            # Reduce epsilon, so progressively we reduce exploration and augment exploitation
            if self.epsilon > 0.1:
                self.epsilon -= (1 / games)
        return losses

    def test(self):
        steps = 0
        game_reward = 0
        observation, info = self.env.reset(seed=self.seed)
        terminated, truncated = False, False
        state = torch.from_numpy(self.preprocess(observation, noise=True)).float()
        while not terminated:
            qval = self.model(state)
            action = np.argmax(qval.data.numpy())
            action_name = action_names[action]
            print(f'Moving {action_name}')
            observation, reward, terminated, truncated, info = self.env.step(action)
            game_reward += reward
            if terminated:
                if reward > 0:
                    print(f"Game won! Reward: {game_reward}s")
                else:
                    print(f"Game LOST. Reward: {game_reward}")
            steps += 1
            if steps > 15:
                print("Game lost; too many moves.")
                break


if __name__ == '__main__':
    human_mode = False
    num_games = 1000
    size = 4
    env = gymnasium.make('GridWorld-v0',
                         render_mode='human' if human_mode else 'rgb_array',
                         size=4,
                         )
    agent = GridWorldQNet(env, seed=42)
    losses = agent.train(num_games)
    plt.figure(figsize=(10, 7))
    plt.plot(losses)
    plt.xlabel("Epochs", fontsize=22)
    plt.ylabel("Loss", fontsize=22)
    plt.show()
    agent.test()
